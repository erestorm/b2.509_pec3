# B2.509 PEC3 - Clon de Sokoban con editor de niveles #

Este proyecto consta de dos componentes:

- Un clon básico de Sokoban
- Un editor de niveles

## Clon de Sokoban ##

Un clon básico de Sokoban con las mecánicas básicas del juego original:

* Movimiento de personaje
* Poder empujar cajas en una cuadrícula
* Casillas “objetivo” donde las cajas deben colocarse

El prototipo consta de 10 niveles de dificultad creciente, creados con el editor de niveles que también se ejecuta dentro de Unity.

### Controles ###

Por defecto (se pueden modificar en el inspector para el objeto Configuration dentro de Unity):

|Tecla|Acción|
|-----|------|
|W o Flecha Arriba|Moverse hacia arriba|
|S o Flecha Abajo|Moverse hacia abajo|
|A o Flecha Izquierda|Moverse hacia la izquierda|
|D o Flecha Derecha|Moverse hacia la derecha|
|Esc|Reiniciar Nivel|
|Esc x 2|Salir del Juego|
|Rueda del ratón|Zoom In/Out|

### Mejoras futuras ###

* Funcionalidad de deshacer (volver atrás en el tiempo)
* Mostrar "Game Over" cuando el nivel está en un estado sin solución posible

## Editor de niveles ##

Herramienta dentro de Unity para crear niveles del clon básico de Sokoban. Permite guardar y cargar niveles para su posterior edición en formato txt.

Los 10 niveles del clon han sido creados y guardados/cargados con esta herramienta.

### Controles ###

Por defecto (se pueden modificar en el inspector para el objeto Configuration dentro de Unity):

|Tecla|Acción|
|-----|------|
|W o Flecha Arriba|Mover la casilla hacia arriba|
|S o Flecha Abajo|Mover la casilla hacia abajo|
|A o Flecha Izquierda|Mover la casilla hacia la izquierda|
|D o Flecha Derecha|Mover la casilla hacia la derecha|
|Tab|Cambiar el tipo de casilla|
|Enter o Espacio|Colocar la casilla actual en la posición actual|
|Borrar o Suprimir|Borrar la casilla que hay la posición actual|
|Enter o Espacio + Movimiento|Colocado rápido|
|Borrar o Suprimir + Movimiento|Borrado rápido|
|Ctrl + S|Salvar el Nivel|
|Ctrl + R|Restaura el Nivel al último estado salvado|
|Ctrl + <Número>|Ir al Nivel <Número>|
|Ctrl + Q|Salir del Juego|
|Rueda del ratón|Zoom In/Out|

### Mejoras futuras ###

* Limitar a un solo avatar, es decir, cuando se coloca un segundo avatar, el primero se borra y se cambia por suelo
* No permitir salvar cuando el número de cajas no es igual al número de huecos
* Dibujar los límites del nivel (por defecto 200x200)

## ¿Cómo se descarga y se usa el proyecto? ##

1. Descargar el .zip [aquí](https://gitlab.com/erestorm/b2.509_pec3/-/archive/master/b2.509_pec3-master.zip)
2. Descomprimirlo
3. Dentro hay tres carpetas:
    * B2.509_PEC3_Game, el clon de Sokoban (proyecto Unity 2018.3.0f2)
    * B2.509_PEC3_LevelEditor, el editor de niveles (proyecto Unity 2018.3.0f2)
    * B2.509_PEC3_Levels, los 10 niveles en formato .txt
4. Copia el contenido de la carpeta B2.509_PEC3_Levels a la carpeta donde apunta [Application.persistentDataPath](https://docs.unity3d.com/ScriptReference/Application-persistentDataPath.html)
    * En Windows, %userprofile%\AppData\LocalLow\Benedicto-UOC\B2_509_PEC3
5. Para usar el juego, abre el proyecto 'B2.509_PEC3_Game' con Unity 2018.3.0f2 y pulsa Ctrl+B
6. Para usar el editor, abre el proyecto 'B2.509_PEC3_LevelEditor' con Unity 2018.3.0f2 y pulsa Ctrl+B

## Créditos ##

Todos los sprites hechos por "Kenney.nl" ([fuente](https://opengameart.org/content/sokoban-100-tiles)).  
Los niveles del 1 al 3 son de https://www.gamesgames.com/game/sokoban-classic, y sirven como tutorial.  
Los niveles del 4 al 10 son los siete primeros niveles del juego original (https://sokoban.info/).  
El tipo de letra usado en la UI es 'MonkeyIsland-1990-refined.ttf' de https://www.ptless.org/sfomi/
