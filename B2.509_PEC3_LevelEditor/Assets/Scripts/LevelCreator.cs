﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible of creating the level.
/// </summary>
public class LevelCreator : MonoBehaviour {

    public GameObject avatarTilePrefab;
    public GameObject boxTilePrefab;
    public GameObject gapTilePrefab;
    public GameObject groundTilePrefab;
    public GameObject wallTilePrefab;

    /// <summary>
    /// Creates the level with the given map.
    /// </summary>
    /// <param name="levelMap">Level map (Array2D of <code>TileType</code> values)</param>
    /// <param name="levelObjects">[output] Level Objects (Array2D of lists of game objects)</param>
    /// <returns>Offset used</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">If one of the tile values is not a valid type</exception>
    public Vector2 CreateLevel(int[,] levelMap, out List<GameObject>[,] levelObjects) {

        int numRows = levelMap.GetLength(0);
        int numCols = levelMap.GetLength(1);

        // Init level objects list
        levelObjects = new List<GameObject>[numRows, numCols];
        for(int row = 0; row < numRows; row++) {
            for(int col = 0; col < numCols; col++) {
                levelObjects[row,col] = new List<GameObject>();
            }
        }

        // Calculate offset so that level is centered in the screen
        Vector2 centerOffset = new Vector2(0.5f * (numCols - 1), 0.5f * (numRows - 1));

        // Draw level
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                int value = levelMap[row, col];

                if (!TileType.IsDefined(typeof(TileType), value)) {
                    Debug.LogError("Value '" + value + "' is not a valid tile type");
                    throw new System.ArgumentOutOfRangeException("Value '" + value + "' is not a valid tile type");
                }

                TileType tileType = (TileType)value;
                if (TileType.Invalid == tileType) {
                    continue;
                }

                levelObjects[row,col].Add(GameObject.Instantiate(GetTilePrefab(tileType), GetTileScreenPosition(row, col, centerOffset), Quaternion.identity));

                if (TileType.Gap != tileType && TileType.Ground != tileType ) {
                    levelObjects[row,col].Add(GameObject.Instantiate(groundTilePrefab, GetTileScreenPosition(row, col, centerOffset), Quaternion.identity));
                }
            }
        }

        return centerOffset;
    }

    public GameObject GetTilePrefab(TileType tileType) {
        GameObject tilePrefab;
        switch (tileType) {
            case TileType.Ground:
                tilePrefab = groundTilePrefab;
                break;
            case TileType.Box:
                tilePrefab = boxTilePrefab;
                break;
            case TileType.Gap:
                tilePrefab = gapTilePrefab;
                break;
            case TileType.Avatar:
                tilePrefab = avatarTilePrefab;
                break;
            case TileType.Wall:
                tilePrefab = wallTilePrefab;
                break;
            default:
                Debug.LogError("TileType '" + tileType + "' not expected");
                throw new System.ArgumentException("TileType '" + tileType + "' not expected");
        }
        return tilePrefab;
    }

    static Vector2 GetTileScreenPosition(int row, int col, Vector2 offset) {
        return new Vector2(col - offset.x, -(row - offset.y));
    }

    public static Vector2Int GetTileCoordinates(float x, float y, Vector2 offset) {
        return new Vector2Int(-(int)(y - offset.y), (int)(x + offset.x));
    }
}
