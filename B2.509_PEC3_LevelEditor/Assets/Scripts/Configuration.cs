﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Provides common configuration which can be edited in the inspector.
/// </summary>
public class Configuration : MonoBehaviour {

    public static readonly char VALUE_SEPARATOR = ',';
    public static readonly string FILE_EXTENSION = ".txt";

    public int maximumNumberOfRows = 200;
    public int maximumNumberOfColumns = 200;
    public float zoomOutCameraSize = 105f;
    public float zoomInCameraSize = 5f;

    public float moveSpeed = 1f;

    public float uiMessageTime = 5f;

    [Header("Editor Controls")]
    public KeyCode[] upKeys = { KeyCode.W, KeyCode.UpArrow };
    public KeyCode[] downKeys = { KeyCode.S, KeyCode.DownArrow };
    public KeyCode[] leftKeys = { KeyCode.A, KeyCode.LeftArrow };
    public KeyCode[] rightKeys = { KeyCode.D, KeyCode.RightArrow };
    public KeyCode[] switchKeys = { KeyCode.Tab };
    public KeyCode[] enterKeys = { KeyCode.Return, KeyCode.KeypadEnter, KeyCode.Space };
    public KeyCode[] deleteKeys = { KeyCode.Backspace, KeyCode.Delete };

    [Header("Control + Key combinations")]
    public KeyCode[] controlKeys = { KeyCode.LeftControl, KeyCode.RightControl };
    public KeyCode[] saveKeys = { KeyCode.S };
    public KeyCode[] quitKeys = { KeyCode.Q };
    public KeyCode[] restartKeys = { KeyCode.R };

    [Header("Zoom Controls")]
    public string zoomAxis = "Mouse ScrollWheel";
}
