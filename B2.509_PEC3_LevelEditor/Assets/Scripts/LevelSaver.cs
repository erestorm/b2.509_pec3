﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Responsible of saving levels to txt files.
/// </summary>
public class LevelSaver : MonoBehaviour {

    Configuration config;

    void Awake() {
        config = GameObject.Find("Configuration").GetComponent<Configuration>();
    }

    /// <summary>
    /// Saves a level.
    /// </summary>
    /// <param>Level map to be saved</param>
    /// <param name="levelNum">Level num</param>
    public void SaveLevel(int[,] levelMap, int levelNum) {
        string levelFilename = "level" + levelNum;
        string levelFilePath = Application.persistentDataPath + Path.DirectorySeparatorChar + levelFilename + Configuration.FILE_EXTENSION;

        int numRows = levelMap.GetLength(0);
        int numCols = levelMap.GetLength(1);

        if(numRows > config.maximumNumberOfRows) {
            Debug.Log("Level has too many rows (max configured is '" + config.maximumNumberOfRows + "')");
            return;
        }
        if(numCols > config.maximumNumberOfColumns) {
            Debug.Log("Level has too many columns (max configured is '" + config.maximumNumberOfColumns + "')");
            return;
        }

        try {
            using (StreamWriter sw = new StreamWriter(levelFilePath)) {
                for(int row = 0; row < numRows; row++) {
                    string line = "";
                    for(int col = 0; col < numCols; col++) {
                        line += levelMap[row,col];
                        if(col != numCols - 1) {
                            line += Configuration.VALUE_SEPARATOR;
                        }
                    }
    	            sw.WriteLine(line);
                }
            }
        } catch (Exception e) {
            Debug.Log("Could not save to '" + levelFilePath + "'");
            Debug.Log(e);
        }
    }
}
