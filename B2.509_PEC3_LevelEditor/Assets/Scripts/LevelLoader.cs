﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Responsible of loading levels from txt files.
/// </summary>
public class LevelLoader : MonoBehaviour {

    Configuration config;

    void Awake() {
        config = GameObject.Find("Configuration").GetComponent<Configuration>();
    }

    /// <summary>
    /// Loads a level by its number.
    /// If a tile value is not in <code>TileType</code>, it is set to <code>Invalid</code>.
    /// It returns an empty array if no file is found for the level.
    /// </summary>
    /// <param name="levelNum">Level num</param>
    /// <returns>Loaded level map</returns>
    public int[,] LoadLevel(int levelNum) {
        string levelFilename = "level" + levelNum;
        string levelFilePath = Application.persistentDataPath + Path.DirectorySeparatorChar + levelFilename + Configuration.FILE_EXTENSION;

        int[,] levelMap = new int[config.maximumNumberOfRows, config.maximumNumberOfColumns];

        try {
            using (StreamReader sr = new StreamReader(levelFilePath)) {
                string line;
                int row = 0;
                while ((line = sr.ReadLine()) != null && line.Length > 0) {
                    if(row >= config.maximumNumberOfRows) {
                        Debug.Log("Level has too many rows (max configured is '" + config.maximumNumberOfRows + "')");
                        break;
                    }
                    string[] values = line.Split(Configuration.VALUE_SEPARATOR);
                    int numCols = values.Length;
                    if (numCols > config.maximumNumberOfColumns) {
                        Debug.Log("Line #" + (row + 1) + " has too many elements (max configured is '" + config.maximumNumberOfColumns + "')");
                        break;
                    }
                    for (int col = 0; col < numCols; col++) {
                        int value;
                        if (int.TryParse(values[col], out value) && TileType.IsDefined(typeof(TileType), value)) {
                            levelMap[row, col] = value;
                        } else {
                            levelMap[row, col] = (int)TileType.Invalid;
                        }
                    }
                    row++;
                }
            }
        } catch (Exception e) {
            Debug.Log("Could not load '" + levelFilePath + "'. Maybe it does not exist?");
            Debug.Log(e);
        }

        return levelMap;
    }
}
