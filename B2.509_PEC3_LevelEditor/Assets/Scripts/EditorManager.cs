﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EditorManager : MonoBehaviour {

    public Transform tile;

    float nextMoveTime = 1f;
    float nextMoveCounter = 0f;
    TileType currentTileType = TileType.Ground;

    static int levelNum = 1;
    string inputLevelNum = "";

    Configuration config;
    LevelLoader levelLoader;
    LevelCreator levelCreator;
    LevelSaver levelSaver;
    UiManager uiManager;

    int[,] levelMap;
    List<GameObject>[,] levelObjects;
    Vector2 levelOffset;

    enum MoveDirection { Up, Down, Left, Right };

    Vector3 cameraOrigin;
    bool zommedIn = true;

    void Awake() {
        config = GameObject.Find("Configuration").GetComponent<Configuration>();
        levelLoader = GameObject.Find("LevelLoader").GetComponent<LevelLoader>();
        levelCreator = GameObject.Find("LevelCreator").GetComponent<LevelCreator>();
        levelSaver = GameObject.Find("LevelSaver").GetComponent<LevelSaver>();
        uiManager = GameObject.Find("UiManager").GetComponent<UiManager>();
    }

    void Start() {
        levelMap = levelLoader.LoadLevel(levelNum);
        levelOffset = levelCreator.CreateLevel(levelMap, out levelObjects);
        uiManager.UpdateLevelName(levelNum);

        cameraOrigin = Camera.main.transform.position;
    }

    void Update() {
        ApplyPlayerInput();
    }

    void ApplyPlayerInput() {
        bool ctrlPressed = false;
        foreach (KeyCode controlKey in config.controlKeys) {
            if (Input.GetKey(controlKey)) {
                ctrlPressed = true;
                break;
            }
        }

        // Ctrl + Number combination
        if (ctrlPressed) {
            for (int num = 0; num < 10; num++) {
                if (Input.GetKeyDown("" + num)) {
                    inputLevelNum += num;
                }
            }
        } else if (!ctrlPressed) {
            if (inputLevelNum.Length > 0) {
                Debug.Log("Going to level " + inputLevelNum);
                levelNum = int.Parse(inputLevelNum);
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        // Ctrl + Key combinations
        foreach (KeyCode saveKey in config.saveKeys) {
            if (ctrlPressed && Input.GetKeyDown(saveKey)) {
                Debug.Log("Saving the level...");
                levelSaver.SaveLevel(levelMap, levelNum);
                uiManager.ShowMessage("LEVEL " + levelNum + " SAVED", config.uiMessageTime);
            }
        }

        foreach (KeyCode restartKey in config.restartKeys) {
            if (ctrlPressed && Input.GetKeyDown(restartKey)) {
                Debug.Log("Restoring level to last saved version...");
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

        foreach (KeyCode quitKey in config.quitKeys) {
            if (ctrlPressed && Input.GetKeyDown(quitKey)) {
                Debug.Log("Quitting the application...");
                Application.Quit();
            }
        }

        // Zoom controls
        if (Input.GetAxis(config.zoomAxis) > 0) {
            Debug.Log("Zooming in...");
            ZoomIn();
        } else if (Input.GetAxis(config.zoomAxis) < 0) {
            Debug.Log("Zooming out...");
            ZoomOut();
        }

        // Tile controls
        foreach (KeyCode switchKey in config.switchKeys) {
            if (!ctrlPressed && Input.GetKeyDown(switchKey)) {
                SwitchTileType();
            }
        }

        bool enterPressed = false;
        foreach (KeyCode enterKey in config.enterKeys) {
            if (!ctrlPressed && Input.GetKeyDown(enterKey)) {
                PlaceTile();
            }
            if (!ctrlPressed && Input.GetKey(enterKey)) {
                enterPressed = true;
            }
        }

        bool deletePressed = false;
        foreach (KeyCode deleteKey in config.deleteKeys) {
            if (!ctrlPressed && Input.GetKeyDown(deleteKey)) {
                DeleteTile();
            }
            if (!ctrlPressed && Input.GetKey(deleteKey)) {
                deletePressed = true;
            }
        }

        // Move controls
        foreach (KeyCode upKey in config.upKeys) {
            if (!ctrlPressed && Input.GetKeyDown(upKey)) {
                nextMoveCounter = 0;
                MoveTile(MoveDirection.Up);
            } else if (!ctrlPressed && Input.GetKey(upKey)) {
                nextMoveCounter += Time.deltaTime * config.moveSpeed;
                if (nextMoveCounter > nextMoveTime) {
                    nextMoveCounter = 0;
                    MoveTile(MoveDirection.Up);
                }
                // Quick tile placement and removal
                if (enterPressed && !deletePressed) {
                    PlaceTile();
                } else if (deletePressed && !enterPressed) {
                    DeleteTile();
                }
            }
        }

        foreach (KeyCode downKey in config.downKeys) {
            if (!ctrlPressed && Input.GetKeyDown(downKey)) {
                nextMoveCounter = 0;
                MoveTile(MoveDirection.Down);
            } else if (!ctrlPressed && Input.GetKey(downKey)) {
                nextMoveCounter += Time.deltaTime * config.moveSpeed;
                if (nextMoveCounter > nextMoveTime) {
                    nextMoveCounter = 0;
                    MoveTile(MoveDirection.Down);
                }
                // Quick tile placement and removal
                if (enterPressed && !deletePressed) {
                    PlaceTile();
                } else if (deletePressed && !enterPressed) {
                    DeleteTile();
                }
            }
        }

        foreach (KeyCode leftKey in config.leftKeys) {
            if (!ctrlPressed && Input.GetKeyDown(leftKey)) {
                nextMoveCounter = 0;
                MoveTile(MoveDirection.Left);
            } else if (!ctrlPressed && Input.GetKey(leftKey)) {
                nextMoveCounter += Time.deltaTime * config.moveSpeed;
                if (nextMoveCounter > nextMoveTime) {
                    nextMoveCounter = 0;
                    MoveTile(MoveDirection.Left);
                }
                // Quick tile placement and removal
                if (enterPressed && !deletePressed) {
                    PlaceTile();
                } else if (deletePressed && !enterPressed) {
                    DeleteTile();
                }
            }
        }

        foreach (KeyCode rightKey in config.rightKeys) {
            if (!ctrlPressed && Input.GetKeyDown(rightKey)) {
                nextMoveCounter = 0;
                MoveTile(MoveDirection.Right);
            } else if (!ctrlPressed && Input.GetKey(rightKey)) {
                nextMoveCounter += Time.deltaTime * config.moveSpeed;
                if (nextMoveCounter > nextMoveTime) {
                    nextMoveCounter = 0;
                    MoveTile(MoveDirection.Right);
                }
                // Quick tile placement and removal
                if (enterPressed && !deletePressed) {
                    PlaceTile();
                } else if (deletePressed && !enterPressed) {
                    DeleteTile();
                }
            }
        }
    }

    void ZoomIn() {
        if (zommedIn) {
            return;
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Camera.main.orthographicSize = config.zoomInCameraSize;
        Camera.main.transform.position = ray.origin;
        zommedIn = true;
    }

    void ZoomOut() {
        if (!zommedIn) {
            return;
        }
        Camera.main.orthographicSize = config.zoomOutCameraSize;
        Camera.main.transform.position = cameraOrigin;
        zommedIn = false;
    }

    void MoveTile(MoveDirection moveDirection) {
        Vector3 newPos;
        Vector3 afterNewPos;
        switch (moveDirection) {
            case MoveDirection.Up:
                newPos = new Vector3(tile.position.x, tile.position.y + 1, tile.position.z);
                afterNewPos = new Vector3(newPos.x, newPos.y + 1, newPos.z);
                break;
            case MoveDirection.Down:
                newPos = new Vector3(tile.position.x, tile.position.y - 1, tile.position.z);
                afterNewPos = new Vector3(newPos.x, newPos.y - 1, newPos.z);
                break;
            case MoveDirection.Left:
                newPos = new Vector3(tile.position.x - 1, tile.position.y, tile.position.z);
                afterNewPos = new Vector3(newPos.x - 1, newPos.y, newPos.z);
                break;
            case MoveDirection.Right:
                newPos = new Vector3(tile.position.x + 1, tile.position.y, tile.position.z);
                afterNewPos = new Vector3(newPos.x + 1, newPos.y, newPos.z);
                break;
            default:
                newPos = tile.position;
                afterNewPos = tile.position;
                break;
        }

        // Check if it is within bounds
        Vector2Int newTileCoordinates = LevelCreator.GetTileCoordinates(newPos.x, newPos.y, levelOffset);
        if (newTileCoordinates.x < 0 || newTileCoordinates.x >= config.maximumNumberOfRows ||
            newTileCoordinates.y < 0 || newTileCoordinates.y >= config.maximumNumberOfColumns) {
            return;
        }

        // Move the tile
        tile.position = newPos;

        // Move the camera if zommed in
        if (zommedIn) {
            Camera.main.transform.position = new Vector3(newPos.x, newPos.y, Camera.main.transform.position.z);
        }
    }

    void SwitchTileType() {
        for (int i = 0; i < tile.childCount; i++) {
            Destroy(tile.GetChild(i).gameObject);
        }

        int maxTileType = (int)Enum.GetValues(typeof(TileType)).Cast<TileType>().Max();
        TileType nextTileType = (TileType)(((int)currentTileType + 1) % (maxTileType + 1));
        if (nextTileType == TileType.Invalid) {
            nextTileType++;
        }
        GameObject.Instantiate(levelCreator.GetTilePrefab(nextTileType), tile.position, Quaternion.identity, tile);
        if (TileType.Gap != nextTileType && TileType.Ground != nextTileType) {
            GameObject.Instantiate(levelCreator.GetTilePrefab(TileType.Ground), tile.position, Quaternion.identity, tile);
        }
        currentTileType = nextTileType;
    }

    void PlaceTile() {
        DeleteTile(); // overwrites

        Vector2Int currentTileCoordinates = LevelCreator.GetTileCoordinates(tile.position.x, tile.position.y, levelOffset);
        levelMap[currentTileCoordinates.x, currentTileCoordinates.y] = (int)currentTileType;
        levelObjects[currentTileCoordinates.x, currentTileCoordinates.y].Add(GameObject.Instantiate(levelCreator.GetTilePrefab(currentTileType), tile.position, Quaternion.identity));
        if (TileType.Gap != currentTileType && TileType.Ground != currentTileType) {
            levelObjects[currentTileCoordinates.x, currentTileCoordinates.y].Add(GameObject.Instantiate(levelCreator.GetTilePrefab(TileType.Ground), tile.position, Quaternion.identity));
        }
    }

    void DeleteTile() {
        Vector2Int currentTileCoordinates = LevelCreator.GetTileCoordinates(tile.position.x, tile.position.y, levelOffset);
        foreach (GameObject tileObject in levelObjects[currentTileCoordinates.x, currentTileCoordinates.y]) {
            Destroy(tileObject);
        }
        levelObjects[currentTileCoordinates.x, currentTileCoordinates.y].Clear();
        levelMap[currentTileCoordinates.x, currentTileCoordinates.y] = (int)TileType.Invalid;
    }
}
