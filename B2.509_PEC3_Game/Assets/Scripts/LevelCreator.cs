﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Responsible of creating the level.
/// </summary>
public class LevelCreator : MonoBehaviour {

    public GameObject avatarTilePrefab;
    public GameObject boxTilePrefab;
    public GameObject gapTilePrefab;
    public GameObject groundTilePrefab;
    public GameObject wallTilePrefab;

    List<Transform> boxes = new List<Transform>();
    Transform avatar;
    
    /// <summary>
    /// Creates the level with the given map.
    /// </summary>
    /// <param name="levelMap">Level map (Array2D of <code>TileType</code> values)</param>
    /// <returns>Offset used</returns>
    /// <exception cref="System.ArgumentOutOfRangeException">If one of the tile values is not a valid type</exception>
    public Vector2 CreateLevel(int[,] levelMap) {

        int numRows = levelMap.GetLength(0);
        int numCols = levelMap.GetLength(1);

        // Calculate offset so that level is centered in the screen
        Vector2 centerOffset = new Vector2(0.5f * (numCols - 1), 0.5f * (numRows - 1));

        // Draw level
        for (int row = 0; row < numRows; row++) {
            for (int col = 0; col < numCols; col++) {
                int value = levelMap[row, col];

                if (!TileType.IsDefined(typeof(TileType), value)) {
                    Debug.LogError("Value '" + value + "' is not a valid tile type");
                    throw new System.ArgumentOutOfRangeException("Value '" + value + "' is not a valid tile type");
                }

                TileType tileType = (TileType)value;
                if (TileType.Invalid == tileType) {
                    continue;
                }

                GameObject tile = GameObject.Instantiate(GetTilePrefab(tileType), GetTileScreenPosition(row, col, centerOffset), Quaternion.identity);

                if (TileType.Gap != tileType && TileType.Ground != tileType ) {
                    GameObject.Instantiate(groundTilePrefab, GetTileScreenPosition(row, col, centerOffset), Quaternion.identity);
                }

                if (TileType.Box == tileType) {
                    boxes.Add(tile.transform);
                    // Replace by ground in level map, we will control its position with the stored transform
                    levelMap[row,col] = (int)TileType.Ground;
                }

                if (TileType.Avatar == tileType) {
                    avatar = tile.transform;
                    // Replace by ground in level map, we will control its position with the stored transform
                    levelMap[row,col] = (int)TileType.Ground;
                }
            }
        }

        return centerOffset;
    }

    /// <summary>
    /// Gets the boxes transforms.
    /// </summary>
    /// <returns>boxes transforms</returns>
    public List<Transform> GetBoxes() {
        return boxes;
    }

    /// <summary>
    /// Gets the avatar transform.
    /// </summary>
    /// <returns>avatar transform</returns>
    public Transform GetAvatar() {
        return avatar;
    }

    GameObject GetTilePrefab(TileType tileType) {
        GameObject tilePrefab;
        switch (tileType) {
            case TileType.Ground:
                tilePrefab = groundTilePrefab;
                break;
            case TileType.Box:
                tilePrefab = boxTilePrefab;
                break;
            case TileType.Gap:
                tilePrefab = gapTilePrefab;
                break;
            case TileType.Avatar:
                tilePrefab = avatarTilePrefab;
                break;
            case TileType.Wall:
                tilePrefab = wallTilePrefab;
                break;
            default:
                Debug.LogError("TileType '" + tileType + "' not expected");
                throw new System.ArgumentException("TileType '" + tileType + "' not expected");
        }
        return tilePrefab;
    }

    static Vector2 GetTileScreenPosition(int row, int col, Vector2 offset) {
        return new Vector2(col - offset.x, -(row - offset.y));
    }

    public static Vector2Int GetTileCoordinates(float x, float y, Vector2 offset) {
        return new Vector2Int(-(int)(y - offset.y), (int)(x + offset.x));
    }
}
