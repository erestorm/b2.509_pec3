﻿using System;
using UnityEngine;

/// <summary>
/// Allows the camera to follow the target (player) if it goes too far.
/// It uses Vector3.Lerp to smooth its movement.
/// </summary>
public class CameraFollow2D : MonoBehaviour {

    public float followSpeed = 0.5f;
    public float maxDistanceX = 5f;
    public float maxDistanceY = 3f;
    public float minDistanceX = 2f;
    public float minDistanceY = 1f;

    Transform target;

    bool moving = false;

    private void Update() {
        if (target == null) {
            GameObject avatar = GameObject.FindGameObjectWithTag("Player");
            if(avatar != null) {
                target = avatar.transform;
            }
        } else {
            float targetDistanceX = Math.Abs(target.position.x - transform.position.x);
            float targetDistanceY = Math.Abs(target.position.y - transform.position.y);

            if (targetDistanceX > maxDistanceX || targetDistanceY > maxDistanceY || moving) {
                moving = true;
                Vector3 newPosition = new Vector3(target.position.x, target.position.y, transform.position.z);
                transform.position = Vector3.Lerp(transform.position, newPosition, followSpeed * Time.deltaTime);
                if(targetDistanceX < minDistanceX && targetDistanceY < minDistanceY) {
                    moving = false;
                }
            }
        }
    }
}
