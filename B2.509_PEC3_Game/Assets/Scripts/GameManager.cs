﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    static int levelNum = 1;

    Configuration config;
    LevelLoader levelLoader;
    LevelCreator levelCreator;
    UiManager uiManager;

    int[,] levelMap;
    Vector2 levelOffset;
    Transform avatar;
    List<Transform> boxes;
    int numBoxesInGap = 0;

    enum MoveDirection { Up, Down, Left, Right };

    Vector3 cameraOrigin;
    bool zommedIn = true;

    bool controlsEnabled = true;
    bool gameStarted = false;

    void Awake() {
        config = GameObject.Find("Configuration").GetComponent<Configuration>();
        levelLoader = GameObject.Find("LevelLoader").GetComponent<LevelLoader>();
        levelCreator = GameObject.Find("LevelCreator").GetComponent<LevelCreator>();
        uiManager = GameObject.Find("UiManager").GetComponent<UiManager>();
    }

    void Start() {
        levelMap = levelLoader.LoadLevel(levelNum);
        levelOffset = levelCreator.CreateLevel(levelMap);
        uiManager.UpdateLevelName(levelNum);

        avatar = levelCreator.GetAvatar();
        boxes = levelCreator.GetBoxes();

        if(avatar != null) {
            Camera.main.transform.position = new Vector3(avatar.position.x, avatar.position.y, Camera.main.transform.position.z);
        }        
        cameraOrigin = Camera.main.transform.position;
    }

    void Update() {
        if (controlsEnabled) {
            ApplyPlayerInput();
        }
        if (LevelCompleted()) {
            controlsEnabled = false;
            LoadNextLevel();
        }
    }

    bool LevelCompleted() {
        return numBoxesInGap == boxes.Count;
    }

    void LoadNextLevel() {
        levelNum++;
        if (!levelLoader.ThereIsLevel(levelNum)) {
            Debug.Log("No next level available, this is the last one.");
            uiManager.ShowMessage("ALL LEVELS COMPLETED!");
            return;
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void ApplyPlayerInput() {
        if (Input.GetAxis(config.zoomAxis) > 0) {
            Debug.Log("Zooming in...");
            ZoomIn();
        } else if (Input.GetAxis(config.zoomAxis) < 0) {
            Debug.Log("Zooming out...");
            ZoomOut();
        }

        foreach (KeyCode upKey in config.upKeys) {
            if (Input.GetKeyUp(upKey)) {
                TryMoveAvatar(MoveDirection.Up);
            }
        }

        foreach (KeyCode downKey in config.downKeys) {
            if (Input.GetKeyUp(downKey)) {
                TryMoveAvatar(MoveDirection.Down);
            }
        }

        foreach (KeyCode leftKey in config.leftKeys) {
            if (Input.GetKeyUp(leftKey)) {
                TryMoveAvatar(MoveDirection.Left);
            }
        }

        foreach (KeyCode rightKey in config.rightKeys) {
            if (Input.GetKeyUp(rightKey)) {
                TryMoveAvatar(MoveDirection.Right);
            }
        }

        foreach (KeyCode restartKey in config.restartKeys) {
            if (Input.GetKeyUp(restartKey)) {
                if(gameStarted) {
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                } else {
                    Debug.Log("Quitting the application...");
                    Application.Quit();
                }
            }
        }
    }

    void ZoomIn() {
        if (zommedIn) {
            return;
        }
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Camera.main.orthographicSize = config.zoomInCameraSize;
        Camera.main.transform.position = ray.origin;
        zommedIn = true;
    }

    void ZoomOut() {
        if (!zommedIn) {
            return;
        }
        Camera.main.orthographicSize = config.zoomOutCameraSize;
        Camera.main.transform.position = cameraOrigin;
        zommedIn = false;
    }

    void TryMoveAvatar(MoveDirection moveDirection) {
        Vector3 newPos;
        Vector3 afterNewPos;
        switch (moveDirection) {
            case MoveDirection.Up:
                newPos = new Vector3(avatar.position.x, avatar.position.y + 1, avatar.position.z);
                afterNewPos = new Vector3(newPos.x, newPos.y + 1, newPos.z);
                break;
            case MoveDirection.Down:
                newPos = new Vector3(avatar.position.x, avatar.position.y - 1, avatar.position.z);
                afterNewPos = new Vector3(newPos.x, newPos.y - 1, newPos.z);
                break;
            case MoveDirection.Left:
                newPos = new Vector3(avatar.position.x - 1, avatar.position.y, avatar.position.z);
                afterNewPos = new Vector3(newPos.x - 1, newPos.y, newPos.z);
                break;
            case MoveDirection.Right:
                newPos = new Vector3(avatar.position.x + 1, avatar.position.y, avatar.position.z);
                afterNewPos = new Vector3(newPos.x + 1, newPos.y, newPos.z);
                break;
            default:
                newPos = avatar.position;
                afterNewPos = avatar.position;
                break;
        }

        Vector2Int currentTileCoordinates = LevelCreator.GetTileCoordinates(avatar.position.x, avatar.position.y, levelOffset);
        Vector2Int newTileCoordinates = LevelCreator.GetTileCoordinates(newPos.x, newPos.y, levelOffset);
        int newTileType = levelMap[newTileCoordinates.x, newTileCoordinates.y];

        // If there is a wall in the next tile
        if (TileType.Wall == (TileType)newTileType) {
            return; // avatar cannot move
        }

        // If there is a box in the new tile
        Transform foundBox;
        if (ThereIsBox(newPos, out foundBox)) {
            // and a wall or a box in the tile after
            Vector2Int afterNewTileCoordinates = LevelCreator.GetTileCoordinates(afterNewPos.x, afterNewPos.y, levelOffset);
            int afterNewTileType = levelMap[afterNewTileCoordinates.x, afterNewTileCoordinates.y];
            Transform foundAnotherBox;
            if (TileType.Wall == (TileType)afterNewTileType || ThereIsBox(afterNewPos, out foundAnotherBox)) {
                return; // nor avatar nor box can move
            }

            // If the box is pushed out of a gap
            if (TileType.Gap == (TileType)newTileType) {
                numBoxesInGap--;
            }

            // If the box is pushed into a gap
            if (TileType.Gap == (TileType)afterNewTileType) {
                numBoxesInGap++; // increment the counter
            }

            // Move the box
            foundBox.position = afterNewPos;
        }

        // Move the avatar
        avatar.position = newPos;

        // Flag the game as started
        gameStarted = true;
    }

    bool ThereIsBox(Vector2 pos, out Transform foundBox) {
        foreach (Transform box in boxes) {
            if (box.position.x == pos.x && box.position.y == pos.y) {
                foundBox = box;
                return true;
            }
        }
        foundBox = null;
        return false;
    }
}
