﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

/// <summary>
/// Responsible of loading levels from txt files.
/// </summary>
public class LevelLoader : MonoBehaviour {

    static char[] LINE_SEPARATORS = { '\r', '\n' };
    static char VALUE_SEPARATOR = ',';

    /// <summary>
    /// Loads a level by its number.
    /// If a tile value is not in <code>TileType</code>, it is set to <code>Invalid</code>.
    /// </summary>
    /// <param name="levelNum">Level number</param>
    /// <returns>Loaded level map</returns>
    public int[,] LoadLevel(int levelNum) {
        string levelFilename = "level" + levelNum;
        string levelFilePath = Application.persistentDataPath + Path.DirectorySeparatorChar + levelFilename + Configuration.FILE_EXTENSION;

        int[,] levelMap = new int[0, 0];
        try {
            using (StreamReader sr = new StreamReader(levelFilePath)) {
                string levelText = sr.ReadToEnd();
                string[] rows = levelText.Split(LINE_SEPARATORS, System.StringSplitOptions.RemoveEmptyEntries);
                int numRows = rows.Length;
                int numCols = rows[0].Split(VALUE_SEPARATOR).Length;
                levelMap = new int[numRows, numCols];

                for (int i = 0; i < numRows; i++) {
                    string[] values = rows[i].Split(VALUE_SEPARATOR);
                    for (int j = 0; j < numCols; j++) {
                        int value;
                        if (int.TryParse(values[j], out value) && TileType.IsDefined(typeof(TileType), value)) {
                            levelMap[i, j] = value;
                        } else {
                            levelMap[i, j] = (int)TileType.Invalid;
                        }
                    }
                }
            }
        } catch (Exception e) {
            Debug.Log("Could not load '" + levelFilePath + "'. Maybe it does not exist?");
            Debug.Log(e);
        }

        return levelMap;
    }

    /// <summary>
    /// Checks if there is a file for a certain level number.
    /// </summary>
    /// <param name="levelNum">Level number</param>
    /// <returns><code>true</code> if level exists, <code>true</code> otherwise</returns>
    public bool ThereIsLevel(int levelNum) {
        string levelFilename = "level" + levelNum;
        string levelFilePath = Application.persistentDataPath + Path.DirectorySeparatorChar + levelFilename + Configuration.FILE_EXTENSION;
        return File.Exists(levelFilePath);
    }
}
