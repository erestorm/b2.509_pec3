﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManager : MonoBehaviour {

    public Text levelName;
    public Text message;

    float currentCountdownValue;

    /// <summary>
    /// Updates the level name to the given level number ("Level" + <code>levelNum</code>).
    /// </summary>
    /// <param name="levelNum">Level number</param>
    public void UpdateLevelName(int levelNum){
        levelName.text = "Level " + levelNum;
    }

    /// <summary>
    /// Shows a message.
    /// </summary>
    /// <param name="text">Message text</param>
    public void ShowMessage(string text) {
        message.text = text;
    }

    /// <summary>
    /// Shows a message for some time.
    /// </summary>
    /// <param name="text">Message text</param>
    /// <param name="seconds">Message time in seconds</param>
    public void ShowMessage(string text, float seconds) {
        ShowMessage(text);
        StartCoroutine(ClearMessageAfter(seconds));
    }

    IEnumerator ClearMessageAfter(float countdownValue) {
        currentCountdownValue = countdownValue;
        while (currentCountdownValue > 0) {
            yield return new WaitForSeconds(1f);
            currentCountdownValue--;
        }
        message.text = "";
    }
}
